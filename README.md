# Control Arai-Usuarios Accounts

Script, desarrollado en Python, que permite la supervisión de los administradores de ARAI-Usuarios y la detección de cualquier modificación en la cantidad de administradores activos. Asimismo, se encarga de monitorear las aplicaciones en relación a las cuentas, notificando si más de un usuario está utilizando la misma cuenta en una misma aplicación. Además, cuenta con la funcionalidad de enviar notificaciones a través de telegram.

> Usted puede usar este script con python-dotenv usando un archivo .env (archivo de configuración) o editando el archivo control-arai-accounts.py y modificar las varibles de la sección de confguraciones línea 34.
>
> El mismo fue testeado en el versión de python 3.9.2 y 3.11.3.

**Clonar el repositorio.**

```bash
git clone https://gitlab.com/cailo/control-arai-accounts.git
```

**Ejecutar el comando.** 

```bash
pip install python-dotenv
```
> Dependiendo de su instalación puede que deba usar pip3 install python-dotenv

**Acceder al directorio control-arai-accounts.**

```bash
cd control-arai-accounts
```

**Copiar .env.dist a .env**

```bash
cp .env.dist .env
```

**Configurar el archivo .env, este archivo contiene las configuraciones para la ejecución del script.**

```bash
nano .env
```

**Copiar aplicaciones.json.dist a aplicaciones.json.** 

```bash
cp aplicaciones.json.dist aplicaciones.json
```

**Configurar el archivo aplicaciones.json, en el mismo se especifican las aplicaciones a monitorear.**

```bash
nano aplicaciones.json
```

**Configurar crontab.**

```bash
crontab -e
```

**Agregando la siguiente línea y editar los datos correspondientes.**

El script se ejecutara cada 15 minutos.

```bash
*/15 * * * * python /path/control-arai-accounts/control-arai-accounts.py
```
> Dependiendo de su instalación puede que deba usar python3

> Para que funcionen las notificaciones el script requiere disponer de un bot de telegram y un grupo.

## Guía para crear un bot en Telegram.
Abrir la aplicación de Telegram y busca al usuario llamado "BotFather".
Iniciar una conversación con BotFather y escribe el comando "/newbot". Siguir las instrucciones y proporciona un nombre y un nombre de usuario para tu bot.
BotFather te proporcionará un token para acceder a la API de Telegram de tu bot. Este token es el usado en la configuración del script.

## Guía para crear un grupo en Telegram y agregar un bot.
Abrir la aplicación de Telegram y pulsar en el icono de "Nuevo Grupo".
Seleccionar los contactos que desees agregar al grupo y pulsar en "Crear".
Ahora que has creado el grupo, pulsar en el nombre del grupo para acceder a su página de información.
Pulsar en "Añadir miembro" y buscar el nombre de usuario del bot. 
Seleccionar el bot y pulsar en "Añadir". Ahora el bot será miembro del grupo.

Para obtener el id del grupo, se puede agregar el bot myidbot.
Una vez que agregado el bot en la conversación del grupo, envía el comando "/getgroupid". 
El bot respondera con el id del grupo, que es usado en la configuración del script.