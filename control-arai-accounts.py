#!/usr/bin/env python3

"""
Script, desarrollado en python, que permite la supervisión de los administradores de ARAI-Usuarios
y la detección de cualquier modificación en la cantidad de administradores activos. Además, se
encarga de monitorear las aplicaciones en relación a las cuentas, notificando si más de un usuario
está utilizando la misma cuenta en una misma aplicación. También cuenta con la funcionalidad de
enviar notificaciones a través de Telegram.
"""

import os
import requests
import json
import logging
from dotenv import load_dotenv
from datetime import datetime


def send_message(title, message, telegram_bot_token, telegram_user_chat_id):
    try:
        telegram_message = f"`{message}`"
        telegram_api_url = f"https://api.telegram.org/bot{telegram_bot_token}/sendMessage"
        payload = {
            "chat_id": telegram_user_chat_id,
            "text": f"*{title}*\n{telegram_message}",
            "parse_mode": "Markdown"
        }
        response = requests.post(telegram_api_url, json=payload)
        # Generar una excepción para códigos de estado diferentes de 200
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        logging.error(
            f"Se produjo un error al enviar el mensaje a Telegram: {str(e)}")
    else:
        logging.info("Mensaje enviado correctamente a Telegram.")


if __name__ == "__main__":
    pathdir = os.path.dirname(__file__)
    logging.basicConfig(filename=f"{pathdir}/control-arai-accounts.log",
                        format=f'%(asctime)s [%(levelname)s] %(message)s',
                        encoding='utf-8',
                        level=logging.DEBUG)
    try:
        load_dotenv()
        url = os.getenv('url')
        apiuser = os.getenv('apiuser')
        apipassw = os.getenv('apipassw')
        identificador_aplicacion_usuarios = os.getenv(
            'identificador_aplicacion_usuarios')
        telegram_bot_token = os.getenv('telegram_bot_token')
        telegram_user_chat_id = os.getenv('telegram_user_chat_id')
        active_admins = int(os.getenv('active_admins'))
        logging.info('Carga de variables de entorno exitosa.')
    except Exception as e:
        logging.error(f'Error al cargar las variables de entorno: {str(e)}')
        exit(1)

    if not os.path.isfile(f"{pathdir}/aplicaciones.json"):
        logging.error(
            f'El archivo de configuración aplicaciones.json no existe!.')
        exit(1)

    try:
        with open(f"{pathdir}/aplicaciones.json", "r") as f:
            data = json.load(f)
    except Exception as e:
        logging.error(f'Error al leer el archivo aplicaciones.json: {str(e)}')
        exit(1)

    current_time = datetime.now().time()
    if current_time >= datetime.strptime("03:50", "%H:%M").time() and current_time < datetime.strptime("04:40", "%H:%M").time():
        logging.info("Script no se ejecuta entre las 03:50 y las 04:40.")
        exit(0)

    telegram_title = "ARAI-Usuarios - Administradores"

    try:
        response = requests.get(f"{url}/aplicaciones/{identificador_aplicacion_usuarios}/cuentas",
                                headers={"Accept-encoding": "gzip"},
                                auth=(apiuser, apipassw))
        response.raise_for_status()

        count_accounts = len(json.loads(response.content))
        list_admin_accounts = json.loads(response.content)

        if count_accounts == active_admins:
            logging.info(
                f"No se han realizado cambios en los administradores de ARAI-Usuarios.")
        else:
            msg_admin = ""
            for admin in list_admin_accounts:
                msg_error = ("", "")
                try:
                    response = requests.get(f"{url}/usuarios/{admin['identificador_usuario']}/atributos",
                                            headers={"Accept-encoding": "gzip"},
                                            auth=(apiuser, apipassw))
                    response.raise_for_status()

                    account_attributes = json.loads(response.content)
                    # print(json.dumps(account_attributes, indent=2))

                    logging.warning(f"Cuenta: {admin['cuenta']} - Usuario: {account_attributes['nombre']} {account_attributes['apellido']} - Identificador: {admin['identificador_usuario']}")
                    msg_admin += f"Cuenta: {admin['cuenta']}\nUsuario: {account_attributes['nombre']} {account_attributes['apellido']}\nIdentificador: {admin['identificador_usuario']}\n\n"

                except requests.exceptions.HTTPError as e:
                    msg_error = f"API-Usuarios: Error al obtener el detalle de la información de los administradores de ARAI-Usuarios. Error HTTP. Ver archivo log."
                    logging.error(f"{msg_error} Error HTTP: {str(e)}")
                    send_message(telegram_title, msg_error + "\n",
                                 telegram_bot_token, telegram_user_chat_id)

                except requests.exceptions.RequestException as e:
                    msg_error = f"API-Usuarios: Error al obtener el detalle de la información de los administradores de ARAI-Usuarios. Error de red. Ver archivo log."
                    logging.error(f"{msg_error} Error de red: {str(e)}")
                    send_message(telegram_title, msg_error + "\n",
                                 telegram_bot_token, telegram_user_chat_id)

                if msg_error[0] != "":
                    msg = f"API-Usuarios: Error al obtener la información del usuario {admin['identificador_usuario']} de la aplicación ARAI-Usuarios. Ver archivo log."
                    msg_admin += f"{msg}\n"
                    logging.error(f"{msg} Error de {msg_error[0]}: {msg_error[1]}")

            if count_accounts < active_admins:
                msg_info = f"Se han eliminado algunos administradores de ARAI-Usuarios, reduciendo su número de {active_admins} a {count_accounts}."
            else:
                msg_info = f"Se han añadido más administradores a ARAI-Usuarios, aumentando su número de {active_admins} a {count_accounts}."

            logging.warning(msg_info)
            m = (f"{msg_info}\n\n{msg_admin}")
            send_message(telegram_title, m, telegram_bot_token,
                         telegram_user_chat_id)

    except requests.exceptions.HTTPError as e:
        msg_error = f"API-Usuarios: Error al obtener la información de los administradores de ARAI-Usuarios. Error HTTP. Ver archivo log."
        logging.error(f"{msg_error} Error HTTP: {str(e)}")
        send_message(telegram_title, msg_error + "\n",
                     telegram_bot_token, telegram_user_chat_id)

    except requests.exceptions.RequestException as e:
        msg_error = f"API-Usuarios: Error al obtener la información de los administradores de ARAI-Usuarios. Error de red. Ver archivo log."
        logging.error(f"{msg_error} Error de red: {str(e)}")
        send_message(telegram_title, msg_error + "\n",
                     telegram_bot_token, telegram_user_chat_id)

    with open(f"{pathdir}/aplicaciones.json", "r") as f:
        data = json.load(f)

    for obj in data:
        aplicacion = obj["aplicacion"]
        identificador_aplicacion = obj["identificador_aplicacion"]
        telegram_title = "ARAI-Usuarios - Cuentas"

        try:
            response = requests.get(f"{url}/aplicaciones/{identificador_aplicacion}/cuentas",
                                    headers={"Accept-encoding": "gzip"},
                                    auth=(apiuser, apipassw))
            response.raise_for_status()

            list_accounts = json.loads(response.content)
            # print(json.dumps(list_accounts, indent=2))

            accounts = {}

            for d in list_accounts:
                key = d['cuenta']
                if key in accounts:
                    accounts[key].append(d)
                else:
                    accounts[key] = [d]
            # print(json.dumps(accounts,indent=2))

            duplicates = []

            for key, cuenta in accounts.items():
                if len(cuenta) > 1:
                    for c in cuenta:
                        duplicates.append(c)

            if len(duplicates) > 1:
                telegram_title = f"Control de Cuentas - {aplicacion}"
                logging.warning(f"En la aplicación {aplicacion}, se ha detectado que varias personas están utilizando la misma cuenta.")
                msg_admin = f"Se ha detectado que varias personas están utilizando la misma cuenta:\n\n"

                for d in duplicates:
                    msg_error = ("", "")
                    try:
                        response = requests.get(f"{url}/usuarios/{d['identificador_usuario']}/atributos",
                                                headers={"Accept-encoding": "gzip"},
                                                auth=(apiuser, apipassw))
                        response.raise_for_status()

                        account_attributes = json.loads(response.content)
                        # print(json.dumps(account_attributes, indent=2))

                        logging.warning(f"Cuenta: {d['cuenta']} - Usuario: {account_attributes['nombre']} {account_attributes['apellido']} - Identificador: {d['identificador_usuario']}")
                        msg_admin += f"Cuenta: {d['cuenta']}\nUsuario: {account_attributes['nombre']} {account_attributes['apellido']}\nIdentificador: {d['identificador_usuario']}\n\n"

                    except requests.exceptions.HTTPError as e:
                        msg_error = f"API-Usuarios: Error al obtener el detalle de la información de las cuentas de una aplicación {aplicacion}:{identificador_aplicacion}. Error HTTP. Ver archivo log."
                        logging.error(f"{msg_error}. Error HTTP: {str(e)}")
                        send_message(telegram_title, msg_error + "\n",
                                     telegram_bot_token, telegram_user_chat_id)

                    except requests.exceptions.RequestException as e:
                        msg_error = f"API-Usuarios: Error al obtener el detalle de la información de las cuentas de la aplicación {aplicacion}:{identificador_aplicacion}. Error de red. Ver archivo log."
                        logging.error(f"{msg_error}. Error de red: {str(e)}")
                        send_message(telegram_title, msg_error + "\n",
                                     telegram_bot_token, telegram_user_chat_id)

                    if msg_error[0] != "":
                        msg = f"API-Usuarios: Error al obtener la información del usuario {d['identificador_usuario']} de la aplicación {aplicacion}. Ver archivo log."
                        msg_admin += f"{msg}\n"
                        logging.error(f"{msg} Error de {msg_error[0]}: {msg_error[1]}")

                send_message(telegram_title, msg_admin,
                             telegram_bot_token, telegram_user_chat_id)

        except requests.exceptions.HTTPError as e:
            msg_error = f"API-Usuarios: Error al obtener la información de las cuentas de una aplicación {aplicacion}:{identificador_aplicacion}. Error HTTP. Ver archivo log."
            logging.error(f"{msg_error}. Error HTTP: {str(e)}")
            send_message(telegram_title, msg_error + "\n",
                         telegram_bot_token, telegram_user_chat_id)

        except requests.exceptions.RequestException as e:
            msg_error = f"API-Usuarios: Error al obtener la información de las cuentas de la aplicación {aplicacion}:{identificador_aplicacion}. Error de red. Ver archivo log."
            logging.error(f"{msg_error}. Error de red: {str(e)}")
            send_message(telegram_title, msg_error + "\n",
                         telegram_bot_token, telegram_user_chat_id)